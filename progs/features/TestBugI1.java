class TestBugI1 {
    public static void main(String[] a) {
	System.out.println(new Bug1().f());
    }
}

class Bug1 {

    public int f() {
           int result;
           boolean x;

           result = 5;
           x = false;
           x++;

           if (x){

               result = 1;  // Boolean incremented

           }else{

               result = 0;  // Not incremented
           }
           return result;
    }
}

